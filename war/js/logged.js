window.onload = function() {

    // add link events
    $('#logout').click(function() {
        console.log("Deleting token...");
        var token = localStorage.getItem('token');
        token = JSON.parse(token);
        console.log("Got token to delete: " + token);
        console.log(token.tokenID);
        console.log(token.user);
        console.log(token.creationData);
        console.log(token.expirationData);
        //localStorage.removeItem('token');

        $.ajax({
            type: "POST",
            url: "../rest/logout",
            contentType: "application/json; charset=utf-8",
            crossDomain: true,
            success: function(response) {
                localStorage.removeItem('token');
                console.log("Token deleted successfully");
                location.href="/index.html";
            },
            error: function(response) {
                alert("Could not logout properly...");
            },
            data: JSON.stringify(token)
        });

        return false;    // to not move to the href
    });

    var token = localStorage.getItem('token');
    console.log(token);

    // Set title
    $("#title").text(localStorage.getItem('email'));

};