var map;

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat:  38.659784, lng:  -9.202765},
        zoom: 16
    });

    var geocoder = new google.maps.Geocoder();

    var getGeo = function(response) {
        if (response) {
            var address = response;
            geocoder.geocode({'address': address.street + " " + address.city + " " + address.CP}, function(results, status) {
                console.log(address.street + " " + address.city + " " + address.CP);
                if (status === google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        'map': map,
                        'position': results[0].geometry.location
                    });
                } else {
                    console.log('Geocode was not successful for the following reason: ' + status);
                }
            });
        } else {
            console.log("Something went wrong?");
        }
    };

    // Function gets the address of given user and shows it on the map
    var getAddress = function(event) {
        var email = $('#email').val();
        console.log("Getting user address: " + email);

        var token = localStorage.getItem('token');
        //token = JSON.parse(token);
        console.log("TOKEN: " + token);
       
        $.ajax({
            type: "POST",
            url: "../rest/operation/" + email,
            contentType: "application/json; charset=utf-8",
            crossDomain: true,
            success: getGeo,
            error: function(response) {
                if (response.status === 403) {
                    console.log("Token out of date. Deleting...");
                    localStorage.removeItem('token');
                    location.href="/index.html";
                } else {
                    alert("Error: "+ response.status);
                }
            },
            data: token     // already stringified
        });
        
        event.preventDefault();
    };

    var getAllGeo = function(response) {
        console.log(response);
        console.log(response.length);

        if (response) {
            var i;
            for (i = 0; i < response.length; i++) {
                var address = response[i];
                console.log(address);
                geocoder.geocode({'address': address.street + " " + address.city + " " + address.CP}, function(results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        var marker = new google.maps.Marker({
                            'map': map,
                            'position': results[0].geometry.location
                        });
                    } else {
                        console.log('Geocode was not successful for the following reason: ' + status);
                    }
                });
            }
        } else {
            console.log("Something went wrong?");
        }
    };

    var getAllAddress = function(event) {
        var token = localStorage.getItem('token');
        //token = JSON.parse(token);
        console.log("TOKEN: " + token);

        $.ajax({
            type: "POST",
            url: "../rest/operation/all",
            contentType: "application/json; charset=utf-8",
            crossDomain: true,
            success: getAllGeo,
            error: function(response) {
                if (response.status === 403) {
                    console.log("Token out of date. Deleting...");
                    localStorage.removeItem('token');
                    location.href="/index.html";
                } else {
                    alert("Error: "+ response.status);
                }
            },
            data: token     // already stringified
        });
        return false;   // prevent href
    };

    var frms = $('form');
    frms[0].onsubmit = getAddress;

    $('#allAddress').click(getAllAddress);
}