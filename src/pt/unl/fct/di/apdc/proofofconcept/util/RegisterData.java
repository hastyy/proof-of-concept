package pt.unl.fct.di.apdc.proofofconcept.util;

public class RegisterData {
	
	public String email;
	public String landlineNumber;
	public String cellphoneNumber;
	public Address address;
	public String NIF;
	public String CC;
	public String password;
	public String confirmation;

	public RegisterData() { }
	
	public RegisterData(String email, String landlineNumber, String cellphoneNumber, Address address, String NIF, String CC, String password, String confirmation) {
		this.email = email;
		this.landlineNumber = landlineNumber;
		this.cellphoneNumber = cellphoneNumber;
		this.address = address;
		this.NIF = NIF;
		this.CC = CC;
		this.password = password;
		this.confirmation = confirmation;
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
	
	public boolean validRegistration() {
		return validField(email) &&
			   validField(password) &&
			   validField(confirmation) &&
			   password.equals(confirmation) &&
			   email.contains("@") &&
			   landlineNumber.matches("\\d+") &&
			   cellphoneNumber.matches("\\d+") &&
			   NIF.matches("\\d+");
	}
	
}
