package pt.unl.fct.di.apdc.proofofconcept.util;

import java.util.UUID;

public class AuthToken {
	
	public static final long EXPIRATION_TIME = 1000 * 60 * 60 * 2;	// 2h
	
	public String user;
	public String tokenID;
	public long creationData;
	public long expirationData;
	
	public AuthToken() { }
	
	public AuthToken(String user) {
		this.user = user;
		this.tokenID = UUID.randomUUID().toString();
		this.creationData = System.currentTimeMillis();
		this.expirationData = this.creationData + AuthToken.EXPIRATION_TIME;
	}
	
	public AuthToken(String user, String tokenID, long creationData, long expirationData) {
		this.user = user;
		this.tokenID = tokenID;
		this.creationData = creationData;
		this.expirationData = expirationData;
	}

}
