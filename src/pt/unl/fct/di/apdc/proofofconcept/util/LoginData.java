package pt.unl.fct.di.apdc.proofofconcept.util;

public class LoginData {
	
	public String email;
	public String password;
	
	public LoginData() { }
	
	public LoginData(String email, String password) {
		this.email = email;
		this.password = password;
	}
	
	public boolean validEmail() {
		return email.contains("@");
	}

}
