package pt.unl.fct.di.apdc.proofofconcept.util;

public class Address {
	
	public String street;
	public String complementary;
	public String city;
	public String CP;	//zipcode
	
	public Address() { }
	
	public Address(String street, String complementary, String city, String CP) {
		this.street = street;
		this.complementary = complementary;
		this.city = city;
		this.CP = CP;
	}

}
