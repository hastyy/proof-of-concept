package pt.unl.fct.di.apdc.proofofconcept.resources;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.proofofconcept.util.AuthToken;
import pt.unl.fct.di.apdc.proofofconcept.util.LoginData;

@Path("/login")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class LoginResource {
	
	// Logger object
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	
	// Converts objects to JSON
	private final Gson g = new Gson();
	
	// Datastore link
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	public LoginResource() { }	// Empty constructor for code 'correctness'
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doLogin(LoginData data) {
		LOG.info("Attempt to login user: " + data.email);
		
		if (!data.validEmail()) {
			LOG.warning("Did not login user " + data.email + " beucase the submitted data was invalid.");
			return Response.status(Status.BAD_REQUEST).entity(g.toJson("Missing or wrong parameter.")).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key userKey = KeyFactory.createKey("User", data.email);
		
		try {
			Entity user = datastore.get(userKey);	// If the entity does not exist an Exception is thrown. Otherwise,
			
			String hashedPWD = (String) user.getProperty("pwd");
			if (hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {
				AuthToken token = new AuthToken(data.email);
				
				Entity tkn = new Entity("Token", token.tokenID, userKey);
				tkn.setIndexedProperty("user", token.user);
				tkn.setIndexedProperty("creationData", token.creationData);
				tkn.setIndexedProperty("expirationData", token.expirationData);
				
				datastore.put(txn, tkn);
				txn.commit();
				
				LOG.info("User '" + data.email + "' logged in sucessfully.");
				return Response.ok(g.toJson(token)).build();
			} else {
				LOG.warning("Wrong password for user: " + data.email);
				txn.rollback();
				return Response.status(Status.FORBIDDEN).build();
			}
		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed login attempt for user: " + data.email);
			txn.rollback();
			return Response.status(Status.FORBIDDEN).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
