package pt.unl.fct.di.apdc.proofofconcept.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.EmbeddedEntity;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.proofofconcept.util.Address;
import pt.unl.fct.di.apdc.proofofconcept.util.AuthToken;

@Path("/operation")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON)
public class OperationResource {
	
	// Logger object
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	
	// Converts objects to JSON
	private final Gson g = new Gson();
	
	// Datastore link
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	public OperationResource() { }	// Empty constructor for code 'correctness'
	
	@POST
	@Path("/{email}")
	public Response getAddress(@PathParam("email") String email, AuthToken token) {
		LOG.info("Attempting to retrieve user's address: " + email);
		
		Key tokenParent = KeyFactory.createKey("User", token.user);
		Key tokenKey = KeyFactory.createKey(tokenParent, "Token", token.tokenID);
		
		try {
			@SuppressWarnings("unused")
			Entity tokenEntity = datastore.get(tokenKey);	// If the entity does not exist an Exception is thrown. Otherwise,
			if (token.expirationData <= System.currentTimeMillis()) {
				LOG.warning("The received token has already expired!");
				return Response.status(Status.FORBIDDEN).build();
			}
		} catch (EntityNotFoundException e1) {
			LOG.warning("The received token has no match in the database!");
			return Response.status(Status.FORBIDDEN).build();
		}
		
		Key userKey = KeyFactory.createKey("User", email);
		
		try {
			Entity user = datastore.get(userKey);	// If the entity does not exist an Exception is thrown. Otherwise,
			EmbeddedEntity adr = (EmbeddedEntity) user.getProperty("address");
			Address address = new Address(
					(String) adr.getProperty("street"),
					(String) adr.getProperty("complementary"),
					(String) adr.getProperty("city"),
					(String) adr.getProperty("cp")
			);
			
			LOG.info("Address successfully retrieved: " + email);
			return Response.ok(g.toJson(address)).build();
		} catch (EntityNotFoundException e) {
			LOG.warning("Failed retrieve user's address: " + email);
			return Response.status(Status.NOT_FOUND).build();
		}
	}
	
	@POST
	@Path("/all")
	public Response getAllAdresses(AuthToken token) {
		LOG.info("Attempting to fetch all adresses from the database....");
		
		Key tokenParent = KeyFactory.createKey("User", token.user);
		Key tokenKey = KeyFactory.createKey(tokenParent, "Token", token.tokenID);
		
		try {
			@SuppressWarnings("unused")
			Entity tokenEntity = datastore.get(tokenKey);	// If the entity does not exist an Exception is thrown. Otherwise,
			if (token.expirationData <= System.currentTimeMillis()) {
				LOG.warning("The received token has already expired!");
				return Response.status(Status.FORBIDDEN).build();
			}
		} catch (EntityNotFoundException e1) {
			LOG.warning("The received token has no match in the database!");
			return Response.status(Status.FORBIDDEN).build();
		}
		
		Query q = new Query("User");
		List<Entity> results = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
		
		if (!results.isEmpty()) {
			List<Address> addresses = new ArrayList<Address>(results.size());
			for (Entity e : results) {
				EmbeddedEntity adr = (EmbeddedEntity) e.getProperty("address");
				addresses.add(new Address(
						(String) adr.getProperty("street"),
						(String) adr.getProperty("complementary"),
						(String) adr.getProperty("city"),
						(String) adr.getProperty("cp")
				));
			}
			
			LOG.info("Successfully got all addresses.");
			return Response.ok(g.toJson(addresses)).build();
		} else {
			LOG.warning("Didn't get any addresses.");
			return Response.status(Status.NOT_FOUND).build();
		}
		
	}

}
