package pt.unl.fct.di.apdc.proofofconcept.resources;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;

@Path("/utils")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class ComputationResource {
	
	// Logger object
	private static final Logger LOG = Logger.getLogger(ComputationResource.class.getName());
	
	// Datastore link
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	public ComputationResource() { }	// Empty constructor for code 'correctness'
	
	@GET
	@Path("/tokens")
	public Response triggerCleanTokensRoutine() {
		Queue queue = QueueFactory.getDefaultQueue();
		queue.add(TaskOptions.Builder.withUrl("/rest/utils/tokens"));
		return Response.ok().build();
	}
	
	@POST
	@Path("/tokens")
	/**
	 * Devia de se fazer a query e as remocoes dentro de uma transacao
	 * No entanto nao o estou a fazer por ter sido incapaz de contornar
	 * os erros da minha implementacao.
	 * @return
	 */
	public Response cleanTokensRoutine() {
		LOG.info("Starting the token cleanup routine...");
		
		//Transaction txn = datastore.beginTransaction();
		Query q = new Query("Token");
		List<Entity> results = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
		long currentTime = System.currentTimeMillis();
		
		for (Entity token : results) {
			long expiration = (long) token.getProperty("expirationData");
			LOG.info("Expiration: " + expiration);
			if (currentTime > expiration) {
				//datastore.delete(txn, token.getKey());
				datastore.delete(token.getKey());
				LOG.info("Token deleted");
			}
		}
		
		//txn.commit();
		
		return Response.ok().build();
	}

}
