package pt.unl.fct.di.apdc.proofofconcept.resources;

import java.util.Date;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.EmbeddedEntity;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.proofofconcept.util.RegisterData;

@Path("/register")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class RegisterResource {
	
	// Logger object
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
		
	// Converts objects to JSON
	private final Gson g = new Gson();
	
	// Datastore link
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	public RegisterResource() { }	// Empty constructor for code 'correctness'
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doRegister(RegisterData data) {
		
		LOG.info("Attempting to register user: " + data.email);
		
		if( ! data.validRegistration() ) {
			LOG.warning("Did not register user " + data.email + " beucase the submitted data was invalid.");
			return Response.status(Status.BAD_REQUEST).entity(g.toJson("Missing or wrong parameter.")).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		try {
			Key userKey = KeyFactory.createKey("User", data.email);
			@SuppressWarnings("unused")
			Entity user = datastore.get(userKey);	// If the entity does not exist an Exception is thrown. Otherwise,
			txn.rollback();
			LOG.warning("Did not register user " + data.email + " because it already exists.");
			return Response.status(Status.BAD_REQUEST).entity(g.toJson("Email already in use.")).build();
		} catch (EntityNotFoundException e) {
			Entity user = new Entity("User", data.email);
			user.setProperty("landline_phone", data.landlineNumber);
			user.setProperty("cellphone", data.cellphoneNumber);
			user.setProperty("nif", data.NIF);
			user.setProperty("cc", data.CC);
			user.setProperty("pwd", DigestUtils.sha512Hex(data.password));
			user.setUnindexedProperty("creation_time", new Date());
			EmbeddedEntity address = new EmbeddedEntity();
			address.setProperty("street", data.address.street);
			address.setProperty("complementary", data.address.complementary);
			address.setProperty("city", data.address.city);
			address.setProperty("cp", data.address.CP);
			user.setProperty("address", address);
			datastore.put(txn,user);
			LOG.info("User registered: " + data.email);
			txn.commit();
			return Response.ok().build();
		} finally {
			if (txn.isActive())
				txn.rollback();
		}
		
	}

}
