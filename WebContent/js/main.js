var captureLoginData = function(event) {
    var data = $('form[name="login"]').jsonify();
    console.log(data);

    $.ajax({
        type: "POST",
        url: "../rest/login",
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        success: function(response) {
            if(response) {
                console.log("Got token with id: " + response.tokenID);
                console.log("Token: " + response);
                // Store token id for later use in localStorage
                localStorage.setItem('token', JSON.stringify(response));
            }
            else {
                console.log("No response");
            }
            var token = localStorage.getItem("token");
            token = JSON.parse(token);
            console.log("Token: " + token);
            console.log(token.tokenID);
            if (token) {
                localStorage.setItem('email', data.email);
                location.href="/logged.html";
            }
        },
        error: function(response) {
            alert("Error: "+ response.status);
        },
        data: JSON.stringify(data)
    });

    event.preventDefault();
};

var captureRegisterData = function(event) {
    var data = $('form[name="register"]').jsonify();
    console.log(data);

    var registerData = {
        email: data.email,
        landlineNumber: data.landline,
        cellphoneNumber: data.cellphone,
        address: {
            street: data.street,
            complementary: data.complementary,
            city: data.city,
            CP: data.zipcode
        },
        NIF: data.nif,
        CC: data.cc,
        password: data.password,
        confirmation: data.confirmation
    };

    $.ajax({
        type: "POST",
        url: "../rest/register",
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        success: function(response) {
            if (response) {
                console.log("Registered successfully!");
            } else {
                console.log("No response "+ response.status);
            }
        },
        error: function(response) {
            alert("Error: "+ response.status);
        },
        data: JSON.stringify(registerData)
    });

    event.preventDefault();
};

var buttonEvents = function(loginForm, registerForm) {
    var loginButton = $('#loginButton');
    var registerButton = $('#registerButton');

    loginButton.click(function() {
        console.log('login btn');
        loginForm.style.display = "block";
        registerForm.style.display = "none";
    });
    registerButton.click(function() {
        console.log('register btn');
        loginForm.style.display = "none";
        registerForm.style.display = "block";
    });
}

window.onload = function() {
    var token = localStorage.getItem("token");
    console.log("Token: " + token);
    if (token)  location.href="/logged.html";

    var frms = $('form');
    console.log(frms);
    frms[0].onsubmit = captureLoginData;
    frms[1].onsubmit = captureRegisterData;
    buttonEvents(frms[0], frms[1]);
}